#include <iostream>
using namespace std;

class Element{
public:
    int data, prio;
    Element* next;
    Element* previous;
    Element(int d, int p){
        data = d;
        prio = p;
        next = NULL;
        previous = NULL;
    }
};

class Queue{
private:
    Element* head;
    Element* tail;
    int maxSize;
    int currentSize;
public:
    void insert (int d, int p);
    void show();
    int remove ();
    Queue (int max){
        maxSize = max;
        head = NULL;
        currentSize = 0;
    }
};

void Queue::insert(int d, int p) {
    Element* newElement = new Element (d, p);
    if (head == NULL){
        head = newElement;
        tail = newElement;
    }
    else{
        if (p>head->prio){
            newElement->next=head;
            head->previous=newElement;
            head = newElement;
        }else{
            Element* temp;
            temp = head;
            while ((temp->next!=NULL) && (p<=temp->next->prio)){
                temp=temp->next;
            }
            Element* sectemp = temp->next;
            temp->next=newElement;
            newElement->previous=temp;
            newElement->next=sectemp;
            if (sectemp != NULL){
                sectemp->previous=newElement;
            }
        }
    }
}

void Queue::show() {
    Element* temp = head;
    while (temp){
    cout<<temp->data;
       temp = temp->next;
    }
}

int Queue::remove() {
    if (head == NULL)
        return -1;
    Element* temp = head;
    head = temp->next;
    delete(temp);
    return 0;
}

int main() {
    Queue* list = new Queue(10);
    std::cout << "Hello, World!" << std::endl;
    list->insert(5, 1);
    list->insert(6, 2);
    list->insert(7, 1);
    list->insert(8, 3);
    list->insert(9, 1);
    //list->remove(1);
    list->show();
    return 0;
}